// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove, onChildAdded, onChildChanged, onChildRemoved } from
    "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

//storage

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyArYylIcouyJ2MwWwUlqzYIEDK2bOh4Usc",
    authDomain: "administradorweb-39a50.firebaseapp.com",
    databaseURL: "https://administradorweb-39a50-default-rtdb.firebaseio.com",
    projectId: "administradorweb-39a50",
    storageBucket: "administradorweb-39a50.appspot.com",
    messagingSenderId: "427033534760",
    appId: "1:427033534760:web:787db29436faa3d30f701d"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Listar Productos
Listarproductos();

function Listarproductos() {
    const section = document.getElementById("secProductos");

    const dbref = refS(db, 'Productos');
    onValue(dbref, (snapshot) => {
        const productosPorCategoria = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const categoria = data.categoria;

            if (!productosPorCategoria[categoria]) {
                productosPorCategoria[categoria] = [];
            }
            productosPorCategoria[categoria].push(data);
        });

        section.innerHTML = '';

        for (const [categoria, productos] of Object.entries(productosPorCategoria)) {
            section.innerHTML += `<hr><h2>${categoria}</h2>`;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card'>
                    <img class='urlImag' src='${producto.urlImag}' alt='' width='100' height='200'>
                    <p class='marca anta-regurar'>${producto.marca}</p>
                    <p class='descripcion'>${producto.descripcion}</p>
                    <p class='talla'>${producto.talla}</p>
                    <button>Agregar al carrito</button>
                </div>`;
            });
        }
    });
}